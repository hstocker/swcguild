var dice1;
var dice2;
var currentBalance = 0;
var slowBalance = 0;
var highWaterMark = 0;
var rollsAtHigh = 0;
var totalRolls = 0;
var ante = 0;
var valid;
var slowValid;

function diceRoll() {
  dice1 = 1 + Math.floor(Math.random() * 6);
  dice2 = 1 + Math.floor(Math.random() * 6);
}

function keepScore() {
  totalRolls++;
  diceRoll();
  if ((dice1 + dice2) == 7) {
    currentBalance += 4;
  } else if ((dice1 + dice2) !== 7) {
    currentBalance -= 1;
  }
  if (currentBalance > highWaterMark) {
    highWaterMark = currentBalance;
    rollsAtHigh = totalRolls;
  }
  displayResults();
}

function slowScore() {
  diceRoll();
  if ((dice1 + dice2) == 7) {
    slowBalance += 4;
  } else if ((dice1 + dice2) !== 7) {
    slowBalance -= 1;
  }
}

function getAnte() {
  ante = parseInt((document.getElementById("anteAmount").value), 10);
  valid = (Number.isInteger(ante));
  if (valid === false)
    alert("Please enter a real integer, $20 - $5000.");
  else if (ante > 500)
    alert("Whoah there, big spender. $500 or less.");
  else if (ante < 20)
    alert("Table minimum $20. Sorry.");
  else if (valid === true)
    currentBalance = ante;
}

//no time to learn "this" so bear with me
function getSlowAnte() {
  var slowAnte;
  slowAnte = parseInt((document.getElementById("slowAmount").value), 10);
  slowValid = (Number.isInteger(slowAnte));
  if (slowValid === false)
    alert("Please enter a real amount, at least $1. Cash in up to $500 at a time.");
  else if (slowAnte >= 500)
    alert("We don't handle chips bigger than $500 at this table.");
  else if (slowAnte === 0)
    alert("Try again with money.");
  else if (slowAnte < 0)
    alert("No refunds, sorry.");
  else if (slowValid === true)
    slowBalance += slowAnte;
  document.getElementById("balance").innerHTML = "$" + slowBalance;
}

function displayResults() {
  document.getElementById("tabStartingBet").innerHTML = ante;
  document.getElementById("tabRolls").innerHTML = totalRolls;
  document.getElementById("tabHighWaterMark").innerHTML = highWaterMark;
  document.getElementById("tabRollsAtBest").innerHTML = rollsAtHigh;
}

function clearResults() {
  ante = 0;
  totalRolls = 0;
  highWaterMark = 0;
  rollsAtHigh = 0;
  currentBalance = 0;
}

function playGame() {
  clearResults();
  getAnte();
  totalRolls = 0;
  while (currentBalance > 0) {
    keepScore();
  }
  displayResults();
  if (totalRolls > 0) {
    document.getElementById("play").setAttribute("value", "Play again!");
  }
}

function graphicDie(result) {
  if (result === 6)
    return "&#9861;";
  else if (result === 5)
    return "&#9860;";
  else if (result === 4)
    return "&#9859;";
  else if (result === 3)
    return "&#9858;";
  else if (result === 2)
    return "&#9857;";
  else if (result === 1)
    return "&#9856;";
}

function rollButton() {
  if (slowBalance > 0) {
    slowScore();
    document.getElementById("firstDie").innerHTML = graphicDie(dice1);
    document.getElementById("secondDie").innerHTML = graphicDie(dice2);
    document.getElementById("balance").innerHTML = "$" + slowBalance;
  } else
    document.getElementById("balance").innerHTML = "You're out of money, partner.";
}

//if there's anything I want credit for, it's getting those panels to toggle
//ugly but they toggle reasonably well on chrome
function hideFast() {
  $(document).ready(function() {
    $("#fastButton").click(function() {
      $("#slowMode").collapse('hide');
    });
  });
}

function hideSlow() {
  $(document).ready(function() {
    $("#slowButton").click(function() {
      $("#fastMode").collapse('hide');
      clearResults();
    });
  });
}
