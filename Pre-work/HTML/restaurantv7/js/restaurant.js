function validateInput() {

  var name = document.getElementById("nameField").value;
  var email = document.getElementById('emailField').value;
  var phone = document.getElementById('phoneField').value;
  var reason = document.getElementById('dropdown').value;
  var info = document.getElementById('customerDescription').value;
  var monday = document.getElementById('m').checked;
  var tuesday = document.getElementById('t').checked;
  var wednesday = document.getElementById('w').checked;
  var thursday = document.getElementById('th').checked;
  var friday = document.getElementById('f').checked;

  if (name === null || name === "") {
    alert("Please enter your name.");
    return false;
  }
  if ((email === null || email === "") && (phone === null || phone == "")) {
    alert("Please enter an email or phone number.")
    return false;
  }
  if ((reason === "other") && (info === null || info === "")) {
    alert("Please submit additional information along with your request.");
    return false;
  }
  if ((monday || tuesday || wednesday || thursday || friday) === false) {
    alert("Please submit a convenient day for us to contact you.");
    return false;
  }
}
